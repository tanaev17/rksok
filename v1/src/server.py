import socket


class TCP_Server:
    def __init__(self, queue, port) -> None:
        self.queue = queue
        self.port = port
        self.server = object

    def create_server(self):
        self.server = socket.create_server(("127.0.0.1", 8000))
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        return self.server

    def start(self):
        try:
            while True:
                client_socket, address = self.server.accept()
                received_data = client_socket.recv(1024).decode('utf-8')
                path = received_data.split()[1]
                response = f"HTTP/1.1 200 OK\nContent-Type: text/html; charset=utf-8\n\n" \
                        f"<br>PATH: {path}"
                client_socket.send(response.encode('utf-8'))
                client_socket.shutdown(socket.SHUT_RDWR)
        except KeyboardInterrupt:
            self.server.shutdown(socket.SHUT_RDWR)
            self.server.close()

start_server = TCP_Server(10, 8000)
start_server.create_server()
start_server.start()
